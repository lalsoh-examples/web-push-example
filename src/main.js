const express = require('express');
const cors = require('cors');
const webPush = require("web-push");
const dotenv = require('dotenv');

dotenv.config();

if (!process.env.VAPID_PUBLIC_KEY || !process.env.VAPID_PRIVATE_KEY) {
  console.log(
    "You must set the VAPID_PUBLIC_KEY and VAPID_PRIVATE_KEY " +
    "environment variables. You can use the following ones:"
  );
  console.log(webPush.generateVAPIDKeys());
  return;
}

webPush.setVapidDetails(
  "https://example.com/",
  process.env.VAPID_PUBLIC_KEY,
  process.env.VAPID_PRIVATE_KEY
);

const app = express();

app.use(express.json());
app.use(cors());
app.use(express.static('public'));

app.get('/api/', (req, res) => {
  res.send({ hello: 'world' });
});

app.get("/api/vapidPublicKey", function(req, res) {
  res.send(process.env.VAPID_PUBLIC_KEY);
});


// each user will have unique subscription data that should be saved 
// so when we need to send  a notification we'll use his subscription data
const subsArray = [];


app.post("/api/register", function(req, res) {
  let subscription = req.body.subscription;

  // A real world application would store the subscription info.
  subsArray.push(subscription);

  console.log({ subscription });
  /*
    {
      subscription: {
        endpoint: 'https://fcm.googleapis.com/fcm/send/cew3RoL2XJU:APA91bEaWrQe6BBEdikz5fQ2SutkJSI-3YxG4awe_lj0jnNuIDzed6ZqMsGCiTVAu5M2gPLwaA6ou5rDlUkR65bf-q5n2BoTueKZT_VGVbxL30gGRXkOCbNKT2EYa4QW9SL4uCWZumZq',
        expirationTime: null,
        keys: {
          p256dh: 'BGYjB_z4SApAg3sL3Z2qajl4OCQTPM4sJ1_wzXFSiq7xTLKT-y6kHJEW6Qf6AcSwW82DoiPNeODgxTh_hLLkqsM',
          auth: 'v6nZvWGq2hpD9G8KTG7OYg'
        }
      }
    }
    */

  res.status(201).send({ registered: true });
});


function sendNotificationNow({ title, body, subscription }) {

  return webPush.sendNotification(
      subscription,
      JSON.stringify({ title, body }),
      {TTL :1 }
    );
}


app.post("/api/sendNotification", function(req, res) {

  const subscription = req.body.subscription;
  const title = req.body.title;
  const body = req.body.body;

  sendNotificationNow({ title, body, subscription })
    .then(function() {
      res.sendStatus(201);
    })
    .catch(function(error) {
      console.log(error);
      res.sendStatus(500);
    });
})


app.listen(process.env.PORT || 4200, () => {
  console.log('push server is running');
});
