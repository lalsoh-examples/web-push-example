# Web push notification example

## Run the project

```sh 
node src/main.js
```

Then open `localhost:4200` in your browser


## Documentation to read:

https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Tutorials/js13kGames/Re-engageable_Notifications_Push

https://www.youtube.com/watch?v=d0XJ9cLfoTs

